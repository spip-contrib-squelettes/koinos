<?php

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

/**
 * Un simple formulaire de config,
 * on a juste à déclarer les saisies
 * @return array
 **/
function formulaires_configurer_koinos_saisies_dist(): array {
	// $saisies est un tableau décrivant les saisies à afficher dans le formulaire de configuration
	$saisies = [
		'options' => ['afficher_si_avec_post' => true],
		[
			'saisie' => 'fieldset',
			'options' => [
				'nom' => 'une',
				'label' => '<:koinos:configurer_une_label:>',
				'onglet' => true,
				'onglet_vertical' => true,
			],
			'saisies' => [
				[
					'saisie' => 'fieldset',
					'options' => [
						'nom' => 'une/generalites',
						'label' => '<:koinos:configurer_une_generalites:>',
					],
					'saisies' => [
						[
							'saisie' => 'input',
							'options' => [
								'nom' => 'une/quantite',
								'label' => '<:koinos:configurer_une_quantite_label:>',
								'defaut' => 2,
								'conteneur_class' => 'pleine_largeur',
								'obligatoire' => 'oui',
							],
							'verifier' => [
								[
									'type' => 'entier',
									'options' => [
										'min' => 0,
										'max' => 2
									]
								]
							]
						],
						[
							'saisie' => 'checkbox',
							'options' => [
								'nom' => 'une/objets',
								'label' =>  '<:koinos:configurer_une_objets:>',
								'conteneur_class' => 'pleine_largeur',
								'data' => [
									'album' => '<:album:titre_albums:>',
									'article' => '<:public:articles:>',
									'evenement' => '<:agenda:evenements:>',
									'document' => '<:medias:media_image:>',
								],
								'afficher_si' => '@une/quantite@ > 0',
							]
						],
						[
							'saisie' => 'radio',
							'options' => [
								'nom' => 'une/mode',
								'label' => '<:koinos:configurer_une_mode_label:>',
								'defaut' => 'mixte',
								'conteneur_class' => 'pleine_largeur',
								'obligatoire' => 'oui',
								'afficher_si' => '@une/quantite@ > 0',
								'data' => [
									'manuel' => '<:koinos:configurer_une_mode_manuel_label:>',
									'auto' => '<:koinos:configurer_une_mode_auto_label:>',
									'mixte' => '<:koinos:configurer_une_mode_mixte_label:>'
								]
							],
						],
					]
				],
				[
					'saisie' => 'fieldset',
					'options' => [
						'nom' => 'une/album',
						'label' => '<:koinos:configurer_une_album_label:>',
						'afficher_si' => '@une/objets@ IN "album" && @une/quantite@ > 0'
					],
					'saisies' => [
						[
							'saisie' => 'input',
							'options' => [
								'nom' => 'une/album/age',
								'label' => '<:koinos:configurer_une_album_age_label:>',
								'explication' => '<:koinos:configurer_une_album_age_explication:>',
								'conteneur_class' => 'pleine_largeur',
								'defaut' => 15,
								'obligatoire' => 'oui',
							],
							'verifier' => [
								[
									'type' => 'entier',
									'options' => [
										'min' => 0
									]
								]
							]
						],
						[
							'saisie' => 'case',
							'options' => [
								'nom' => 'une/album/auto',
								'defaut' => 'on',
								'conteneur_class' => 'pleine_largeur',
								'label_case' =>  '<:koinos:configurer_une_album_auto_label_case:>',
								'afficher_si' => '@une/mode@ != "manuel"'
							]
						]
					]
				],
				[
					'saisie' => 'fieldset',
					'options' => [
						'nom' => 'une/article',
						'label' => '<:koinos:configurer_une_article_label:>',
						'afficher_si' => '@une/objets@ IN "article" && @une/quantite@ > 0'
					],
					'saisies' => [
						[
							'saisie' => 'input',
							'options' => [
								'nom' => 'une/article/age',
								'label' => '<:koinos:configurer_une_article_age_label:>',
								'explication' => '<:koinos:configurer_une_article_age_explication:>',
								'conteneur_class' => 'pleine_largeur',
								'defaut' => 15,
								'obligatoire' => 'oui',
							],
							'verifier' => [
								[
									'type' => 'entier',
									'options' => [
										'min' => 0
									]
								]
							]
						],
						[
							'saisie' => 'case',
							'options' => [
								'nom' => 'une/article/auto',
								'defaut' => 'on',
								'conteneur_class' => 'pleine_largeur',
								'label_case' =>  '<:koinos:configurer_une_article_auto_label_case:>',
								'afficher_si' => '@une/mode@ != "manuel"'
							]
						]
					]
				],
				[
					'saisie' => 'fieldset',
					'options' => [
						'nom' => 'une/document',
						'label' => '<:koinos:configurer_une_document_label:>',
						'afficher_si' => '@une/objets@ IN "document" && @une/quantite@ > 0'
					],
					'saisies' => [
						[
							'saisie' => 'input',
							'options' => [
								'nom' => 'une/document/age',
								'label' => '<:koinos:configurer_une_document_age_label:>',
								'explication' => '<:koinos:configurer_une_document_age_explication:>',
								'conteneur_class' => 'pleine_largeur',
								'defaut' => 15,
								'obligatoire' => 'oui',
							],
							'verifier' => [
								[
									'type' => 'entier',
									'options' => [
										'min' => 0
									]
								]
							]
						],
						[
							'saisie' => 'case',
							'options' => [
								'nom' => 'une/document/auto',
								'conteneur_class' => 'pleine_largeur',
								'label_case' =>  '<:koinos:configurer_une_document_auto_label_case:>',
								'afficher_si' => '@une/mode@ != "manuel"'
							]
						]
					]
				],
				[
					'saisie' => 'fieldset',
					'options' => [
						'nom' => 'une/evenement',
						'label' => '<:koinos:configurer_une_evenement_label:>',
						'afficher_si' => '@une/objets@ IN "evenement" && @une/quantite@ > 0'
					],
					'saisies' => [
						[
							'saisie' => 'input',
							'options' => [
								'nom' => 'une/evenement/limite_debut_futur',
								'label' => '<:koinos:configurer_une_evenement_limite_debut_futur_label:>',
								'conteneur_class' => 'pleine_largeur',
								'defaut' => 15,
								'obligatoire' => 'oui',
							],
							'verifier' => [
								[
									'type' => 'entier',
									'options' => [
										'min' => 0
									]
								]
							]
						],
						[
							'saisie' => 'case',
							'options' => [
								'nom' => 'une/evenement/auto',
								'conteneur_class' => 'pleine_largeur',
								'label_case' =>  '<:koinos:configurer_une_evenement_auto_label_case:>',
								'afficher_si' => '@une/mode@ != "manuel"'
							]
						]
					]
				]
			]
		],
		[
			'saisie' => 'fieldset',
			'options' => [
				'nom' => 'descriptif',
				'label' => '<:koinos:configurer_descriptif_label:>',
				'onglet' => true,
				'onglet_vertical' => true,
			],
			'saisies' => [
				[
					'saisie' => 'textarea',
					'options' => [
						'nom' => 'descriptif/agenda',
						'label' => '<:koinos:configurer_descriptif_agenda_label:>',
						'conteneur_class' => 'pleine_largeur',
						'rows' => 5
					]
				],
				[
					'saisie' => 'textarea',
					'options' => [
						'nom' => 'descriptif/albums',
						'label' => '<:koinos:configurer_descriptif_albums_label:>',
						'conteneur_class' => 'pleine_largeur',
						'rows' => 5
					]
				]
			]
		],
		[
			'saisie' => 'fieldset',
			'options' => [
				'nom' => 'lateral',
				'label' => '<:koinos:configurer_lateral_label:>',
				'onglet' => true,
			],
			'saisies' => [
				[
					'saisie' => 'input',
					'options' => [
						'nom' => 'lateral/fil/pagination',
						'label' => '<:koinos:configurer_lateral_fil_pagination_label:>',
						'obligatoire' => 'on'
					],
					'verifier' => [
						'type' => 'entier',
						'options' => [
							'min' => 0,
						]
					]
				],
				[
					'saisie' => 'input',
					'options' => [
						'nom' => 'lateral/fil/age',
						'label' => '<:koinos:configurer_lateral_fil_age_label:>',
						'obligatoire' => 'on'
					],
					'verifier' => [
						'type' => 'entier',
						'options' => [
							'min' => 0,
						]
					]
				],
			]
		],
		[
			'saisie' => 'fieldset',
			'options' => [
				'nom' => 'banniere',
				'label' => '<:koinos:configurer_banniere_label:>',
				'onglet' => true,
				'onglet_vertical' => true,
			],
			'saisies' => [
				[
					'saisie' => 'case',
					'options' => [
						'nom' => 'banniere/sommaire_seulement',
						'conteneur_class' => 'pleine_largeur',
						'label_case' => '<:koinos:configurer_banniere_sommaire_seulement_label_case:>',
					]
				]
			]
		]
	];
	return $saisies;
}
