<?php

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

/**
 * Déclaration de cextras
 * @param array $champs
 * @return array $champs
**/
function koinos_declarer_champs_extras(array $champs = []): array {

	foreach (['album', 'article', 'document', 'evenement'] as $objet) {
		$table = "spip_{$objet}s";// On verra si un jour on a de l'irrégulier
		$saisie_une = [
			'saisie' => 'case',
			'options' => [
				'nom' => 'une',
				'label_case' => _T('koinos:cextra_une_label_case'),
				'explication' => _T('koinos:cextra_une_explication'),
				'conteneur_class' => 'pleine_largeur',
				'sql' => 'varchar(2) NOT NULL DEFAULT \'\'',
				'afficher_si' => "@config:koinos/une/mode@ != 'auto' && @config:koinos/une/objets@ IN '$objet'",
				'restrictions' => [
					'modifier' => ['auteur' => 'admin_complet']//Seuls les admins peuvent modifier
				],
				'versionner' =>  true,
			]
		];
		$champs[$table]['une'] = $saisie_une;
	}

	$champs['spip_articles']['voir_aussi'] = [
		'saisie' => 'case',
		'options' => [
			'nom' => 'voir_aussi',
			'label_case' => _T('koinos:cextra_voir_aussi_label_case'),
			'conteneur_class' => 'pleine_largeur',
			'sql' => 'varchar(2) NOT NULL DEFAULT \'\'',
			'restrictions' => [
				'modifier' => ['auteur' => 'admin_complet']//Seuls les admins peuvent modifier
			],
			'versionner' =>  true,
		]
	];
  return $champs;
}
