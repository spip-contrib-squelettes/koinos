<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de https://trad.spip.net/tradlang_module/koinos-paquet-xml-koinos?lang_cible=es
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// K
	'koinos_description' => 'Esqueleto para presentar una comunidad de habitantes.',
	'koinos_slogan' => 'Trabajemos en común !'
);
