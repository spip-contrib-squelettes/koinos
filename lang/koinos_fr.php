<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// Fichier source, a modifier dans https://git.spip.net/spip-contrib-squelettes/koinos.git
if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// A
	'activite' => 'Activité',
	'activites' => 'Activités',
	'afficher_formulaire_forum' => 'Afficher le formulaire d’ajout d’actualité',
	'album_koinos' => 'Prévisualisation d’un album',
	'albums' => 'Albums',
	'aller_agenda' => 'Aller à l’agenda',
	'aller_aside' => 'Aller au menu secondaire',
	'aller_content' => 'Aller au contenu principal',
	'aller_nav' => 'Aller au menu principal',
	'aller_recherche' => 'Aller à la recherche',
	'aller_sur_le_fil' => 'Aller aux dernières nouvelles',
	'annees' => 'Années',
	'auteurs_albums' => 'Artistes',

	// C
	'cextra_une_explication' => 'Sous réserve des limites de quantité et de date configurées pour l’ensemble du site.',
	'cextra_une_label_case' => 'À la une',
	'cextra_voir_aussi_label_case' => 'Article dans la colonne « Voir aussi »',
	'composition_about_description' => 'Articles d’information sur le site',
	'composition_about_nom' => 'À propos du site',
	'composition_annuaire_description' => 'Annuaire des structures locales',
	'composition_annuaire_nom' => 'Annuaire',
	'configurer' => 'Configurer le squelette Koinós',
	'configurer_banniere_label' => 'Bannière',
	'configurer_banniere_sommaire_seulement_label_case' => 'La bannière est visible uniquement sur la page d’accueil',
	'configurer_descriptif_agenda_label' => 'Présentation de l’agenda',
	'configurer_descriptif_albums_label' => 'Présentation de la page des albums',
	'configurer_descriptif_label' => 'Descriptifs des pages spéciales',
	'configurer_lateral_fil_age_label' => 'Age maximum des élèments sur le fil',
	'configurer_lateral_fil_pagination_label' => 'Pagination des élèments sur le fil',
	'configurer_lateral_label' => 'Colonne latérale',
	'configurer_une_album_age_explication' => 'La date de publication est le point de référence.',
	'configurer_une_album_age_label' => 'Âge maximum d’un album en une (jours)',
	'configurer_une_album_auto_label_case' => 'Les albums peuvent être mis automatiquement à la une',
	'configurer_une_album_label' => 'Albums à la une',
	'configurer_une_article_age_explication' => 'La date de publication est le point de référence.',
	'configurer_une_article_age_label' => 'Âge maximum d’un article en une (jours)',
	'configurer_une_article_auto_label_case' => 'Les articles peuvent être mis automatiquement à la une',
	'configurer_une_article_label' => 'Articles à la une',
	'configurer_une_document_age_explication' => 'La date de modification est le point de référence.',
	'configurer_une_document_age_label' => 'Âge maximum de l’image',
	'configurer_une_document_auto_label_case' => 'Les images peuvent être mises automatiquement à la une',
	'configurer_une_document_label' => 'Images à la une',
	'configurer_une_evenement_auto_label_case' => 'Les évènements peuvent être mis automatiquement à la Une',
	'configurer_une_evenement_label' => 'Évènements à la une',
	'configurer_une_evenement_limite_debut_futur_label' => 'Jusqu’à combien de jours dans le futur peuvent commencer les évènements à la une',
	'configurer_une_generalites' => 'Généralités',
	'configurer_une_label' => 'Une',
	'configurer_une_mode_auto_label' => 'Automatique',
	'configurer_une_mode_label' => 'Sélection des objets à la une',
	'configurer_une_mode_manuel_label' => 'Manuel',
	'configurer_une_mode_mixte_label' => 'Manuel, puis sélection automatique si besoin',
	'configurer_une_objets' => 'Objets pouvant être en Une',
	'configurer_une_quantite_label' => 'Nombre d’objets en Une',
	'contact' => 'Contact',
	'coordonnes' => 'Coordonnées',
	'criteres_tri' => 'Trier par',

	// D
	'date' => 'Date',
	'defaut' => 'Défaut',

	// E
	'encours' => 'En cours',
	'evenements' => 'Événements',

	// F
	'forums' => 'Sur le fil',

	// J
	'journal' => 'Journal',

	// M
	'masquer_formulaire_forum' => 'Masquer le formulaire d’ajout d’actualité',
	'mediatheque' => 'Médiathèque',
	'misajour' => 'Mis à jour le',
	'motsconnexes' => 'Mots connexes',

	// O
	'oups' => 'Oups !',

	// P
	'plus_images' => '+ d’images',
	'prochainement' => 'Prochainement',
	'publiele' => 'Publié le',
	'publiepar' => 'Par',

	// R
	'remonter' => 'Remonter',

	// S
	'sous_rubriques' => 'Sous-rubriques',

	// U
	'une' => 'À la une',

	// V
	'voir_aussi' => 'Voir aussi',
	'voiraussi' => 'Voir aussi',
	'votreadresseelectronique' => 'courriel@fournisseur'
);
