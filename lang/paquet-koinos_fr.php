<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// Fichier source, a modifier dans https://git.spip.net/spip-contrib-squelettes/koinos.git
if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// K
	'koinos_description' => 'Squelette pour présenter une communauté d’habitant·es.',
	'koinos_slogan' => 'Faisons commun !'
);
