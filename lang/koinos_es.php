<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de https://trad.spip.net/tradlang_module/koinos-koinos?lang_cible=es
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// A
	'activite' => 'Actividad',
	'activites' => 'Actividades',
	'afficher_formulaire_forum' => 'Mostrar el formulario para añadir noticias',
	'albums' => 'Álbumes',
	'aller_agenda' => 'Ir a la agenda',
	'aller_aside' => 'Ir al menú secundario',
	'aller_content' => 'Ir al contenido principal',
	'aller_nav' => 'Ir al menú principal',
	'aller_recherche' => 'Ir a búsqueda',
	'aller_sur_le_fil' => 'Ir a últimas noticias',
	'annees' => 'Años',
	'auteurs_albums' => 'Artistas',

	// C
	'cextra_une_explication' => 'Sujeto a límites de cantidad y fecha establecidos para el conjunto del sitio.',
	'cextra_une_label_case' => 'En portada',
	'cextra_voir_aussi_label_case' => 'Artículo en la columna "Ojeando"',
	'composition_about_description' => 'Artículos de información en el sitio',
	'composition_about_nom' => 'A cerca del sitio',
	'composition_annuaire_description' => 'Directorio de colectivos locales',
	'composition_annuaire_nom' => 'Directorio',
	'configurer' => 'Configurar el esqueleto Koinós',
	'configurer_banniere_label' => 'Banner',
	'configurer_banniere_sommaire_seulement_label_case' => 'El banner sólo es visible en la página de inicio',
	'configurer_descriptif_agenda_label' => 'Presentación de la agenda',
	'configurer_descriptif_albums_label' => 'Presentación de la página de álbumes',
	'configurer_descriptif_label' => 'Descripciones de páginas especiales',
	'configurer_lateral_fil_age_label' => 'Tiempo máximo de los elementos en "Al hilo"',
	'configurer_lateral_fil_pagination_label' => 'Paginación de elementos en columna "Al hilo"',
	'configurer_lateral_label' => 'Columna lateral',
	'configurer_une_album_age_explication' => 'La fecha de publicación es el punto de referencia.',
	'configurer_une_album_age_label' => 'Tiempo máximo de un álbum en Portada (días)',
	'configurer_une_album_auto_label_case' => 'Los álbumes pueden ser puestos automáticamente en Portada',
	'configurer_une_album_label' => 'Álbumes en Portada',
	'configurer_une_article_age_explication' => 'La fecha de publicación es el punto de referencia.',
	'configurer_une_article_age_label' => 'Tiempo máximo de un artículo en Portada (días)',
	'configurer_une_article_auto_label_case' => 'Los artículos pueden incluirse automáticamente en Portada',
	'configurer_une_article_label' => 'Artículos en Portada',
	'configurer_une_document_age_explication' => 'La fecha de modificación es el punto de referencia.',
	'configurer_une_document_age_label' => 'Tiempo máximo de la imagen',
	'configurer_une_document_auto_label_case' => 'Las imágenes pueden incluirse automáticamente en Portada',
	'configurer_une_document_label' => 'Imágenes en Portada',
	'configurer_une_evenement_auto_label_case' => 'Los eventos pueden incluirse automáticamente en Portada',
	'configurer_une_evenement_label' => 'Eventos en Portada',
	'configurer_une_evenement_limite_debut_futur_label' => 'Hasta cuántos días en el futuro pueden comenzar los eventos en Portada',
	'configurer_une_generalites' => 'General',
	'configurer_une_label' => 'Portada',
	'configurer_une_mode_auto_label' => 'Automático',
	'configurer_une_mode_label' => 'Selección de objetos en Portada',
	'configurer_une_mode_manuel_label' => 'Manual',
	'configurer_une_mode_mixte_label' => 'Selección manual y automática en caso necesario',
	'configurer_une_objets' => 'Objetos que pueden estar en Portada',
	'configurer_une_quantite_label' => 'Número de objetos en Portada',
	'contact' => 'Contacto',
	'coordonnes' => 'Datos de contacto',
	'criteres_tri' => 'Ordenar por',

	// D
	'date' => 'Fecha',
	'defaut' => 'Predeterminado',

	// E
	'encours' => 'En curso',
	'evenements' => 'Eventos',

	// F
	'forums' => 'Al hilo',

	// J
	'journal' => 'Titulares',

	// M
	'masquer_formulaire_forum' => 'Ocultar el formulario de añadir noticias',
	'mediatheque' => 'Mediateca',
	'misajour' => 'Actualizado el',
	'motsconnexes' => 'Palabras relacionadas',

	// O
	'oups' => '¡ Uy !',

	// P
	'plus_images' => '+  Imágenes',
	'prochainement' => 'Próximamente',
	'publiele' => 'El', # MODIF
	'publiepar' => 'Por',

	// R
	'remonter' => 'Subir ',

	// S
	'sous_rubriques' => 'Subsecciones ',

	// U
	'une' => 'Portada',

	// V
	'voir_aussi' => 'Ojeando',
	'voiraussi' => 'Ojeando',
	'votreadresseelectronique' => 'correo@contacto'
);
