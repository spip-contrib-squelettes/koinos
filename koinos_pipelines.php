<?php

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

/**
 * Ajouter un placeholder dans le formulaire d'incription à la newsletter
 **/
function koinos_formulaire_fond(array $flux): array {
	if ($flux['args']['form'] === 'newsletter_subscribe') {
		$placeholder = 'placeholder="' . _T('koinos:votreadresseelectronique') . '"';
		$id = 'id="session_email"';
		$flux['data'] = str_replace($id, "$id $placeholder", $flux['data']);
	}
	return $flux;
}
