# Changelog

## Unreleased

### Fixed
- #40 Recharger entièrement la page après ajout d'une actualisation
- #39 Coherence d'affichage des dates de publication et de modification dans les articles
- #38 Affichage uniforme des boutons de modification côté espace public

## 2.2.0 - 2024-03-13

### Added

- Possibilité d'insérer dans un article une prévisualisation d'un album
- #37 Insertion automatique des albums liés à un article en portfolio

### Fixed

- Afficher les webp dans le portfolio

### Removed

- Plus de pagination sur les documents en portfolio
## 2.1.3 2024-01-30

### Fixed

- Activer le portfolio sur les albums

## 2.1.2 - 2023-12-26

### Fixed

- Bug CSS sur l'agenda sur petit smartphone


## 2.1.1 - 2023-08-15

### Fixed

- Compatibilité de la UNE avec SQLite
## 2.1.0 - 2023-07-26

### Added

- #14 Possibilité de supprimer un élément de fil d'actu depuis l'espace public

### Fixed

- #20 Layout vraiment responsive
- Divers ajustement css (padding et margin)
- La bannière toujours en pleine largeur
- #25 Ne pas couper les titres

### Changed

- #9 Pour présenter un article, prendre l'introduction explicite (`<intro>...</intro>`), à défaut le chapeau, à défaut l'introduction automatique
- #10 Ne pas mettre d'introduction pour les articles de voir aussi

## 2.0.1 - 2023-07-09
### Fixed
- #8 Ne pas recadrer les images à la une
- Trier correctement les objets en une
- Sélectionner correctement les objets de une de rattrapage
- #5 Ne pas être redirigé aléatoirement après connexion
- En l'absence de slogan, pas de tiret en bas de page
- #11 Exclure les articles d'annuaire de la une automatique, suppose que l'annuaire soit bien à la racine du site
## 2.0.0 - 2023-06-16

### Added

- Première sortie officielle
