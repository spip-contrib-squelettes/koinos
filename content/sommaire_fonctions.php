<?php

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

/**
 * Sélectionner les objets à la une, selon la config
 * @return array [['objet' => str, 'id_objet' => 'int'],...]
 **/
function koinos_determiner_objets_une(): array {
	$une = [];
	include_spip('inc/config');
	include_spip('inc/sql');
	$where_statut = 'statut ='.sql_quote('publie');

	// Les principaux paramètres
	$config = lire_config('koinos/une');
	$quantite = intval($config['quantite']);
	$objets = $config['objets'];
	$mode = $config['mode'];

	$objets_a_trouver = $quantite;


	// D'abord trouver ce qui est explicitement marqué comme une
	if (in_array($mode, ['mixte', 'manuel']) && $objets_a_trouver) {
		if (in_array('article', $objets)) {
			$age = $config['article']['age'];
			$date = new DateTime();
			$date->modify("- $age day");
			$date = 'date(\'' . $date->format('Y-m-d H:i:s') .'\')';
			$une = array_merge($une, sql_allfetsel('id_article AS id_objet, date, \'article\' AS objet', 'spip_articles', "$where_statut and date >= $date AND une=" . sql_quote('on'), '', 'date DESC', "0, $objets_a_trouver"));
		}

		if (in_array('album', $objets)) {
			$age = $config['album']['age'];
			$date = new DateTime();
			$date->modify("- $age day");
			$date = 'date(\'' . $date->format('Y-m-d H:i:s') .'\')';
			$une = array_merge($une, sql_allfetsel('id_album AS id_objet, date, \'album\' AS objet', 'spip_albums', "$where_statut and date >= $date AND une=" . sql_quote('on'), '', 'date DESC', "0, $objets_a_trouver"));
		}

		if (in_array('document', $objets)) {
			$age = $config['document']['age'];
			$date = new DateTime();
			$date->modify("- $age day");
			$date = 'date(\'' . $date->format('Y-m-d H:i:s') .'\')';
			$une = array_merge($une, sql_allfetsel('id_document AS id_objet, maj as date, \'image\' AS objet', 'spip_documents', "maj >= $date AND media=" . sql_quote('image') . ' AND une= ' . sql_quote('on'), '', 'date DESC', "0, $objets_a_trouver"));
		}

		if (in_array('evenement', $objets)) {
			$limite_debut_futur = $config['evenement']['limite_debut_futur'];
			$date = new DateTime();
			$date->modify("+ $limite_debut_futur day");
			$date = 'date(\'' . $date->format('Y-m-d H:i:s') .'\')';
			$une = array_merge($une, sql_allfetsel('id_evenement AS id_objet, date_debut as date, \'evenement\' AS objet', 'spip_evenements', "$where_statut and date_debut <= $date AND date_debut > NOW() AND une=" . sql_quote('on'), '', 'date DESC', "0, $objets_a_trouver"));
		}
	}

	// Puis trier
	usort($une, function ($a, $b) {
		if ($a['date'] < $b['date']) {
			return 1;
		} elseif ($a['date'] === $b['date']) {
			return 0;
		} else {
			return -1;
		}
	});

	// Et limiter
	$une = array_slice($une, 0, $quantite);
	// Combien reste-il d'objet à trouver
	$objets_a_trouver = $quantite - count($une);

	// Puis trouver ce qui n'est pas explicitement marqué comme comme une
	if (in_array($mode, ['mixte', 'auto']) && $objets_a_trouver) {
		if (in_array('article', $objets) && $config['article']['auto'] ?? '') {
			$age = $config['article']['age'];
			$date = new DateTime();
			$date->modify("- $age day");
			$date = 'date(\'' . $date->format('Y-m-d H:i:s') .'\')';
			include_spip('compositions_fonctions');
			$rubriques_annuaire = array_column(compositions_lister_utilisations('rubrique', 'annuaire'), 'id');
			$une = array_merge($une, sql_allfetsel(
				'id_article AS id_objet, date, \'article\' AS objet',
				'spip_articles',
				[
					$where_statut,
					"date >= $date",
					sql_in('id_secteur', $rubriques_annuaire, 'NOT'),
					sql_in('id_rubrique', $rubriques_annuaire, 'NOT'),
					'une!=' . sql_quote('on')
				],
				'',
				'date DESC',
				"0,
				$objets_a_trouver")
			);
		}

		if (in_array('album', $objets) && $config['album']['auto'] ?? '') {
			$age = $config['album']['age'];
			$date = new DateTime();
			$date->modify("- $age day");
			$date = 'date(\'' . $date->format('Y-m-d H:i:s') .'\')';
			$une = array_merge($une, sql_allfetsel('id_album AS id_objet, date, \'album\' AS objet', 'spip_albums', "$where_statut and date >= $date AND une!=" . sql_quote('on'), '', 'date DESC', "0, $objets_a_trouver"));
		}

		if (in_array('document', $objets) && $config['document']['auto'] ?? '') {
			$age = $config['document']['age'];
			$date = new DateTime();
			$date->modify("- $age day");
			$date = 'date(\'' . $date->format('Y-m-d H:i:s') .'\')';
			$une = array_merge($une, sql_allfetsel('id_document AS id_objet, maj as date, \'image\' AS objet', 'spip_documents', "maj >= $date AND media=" . sql_quote('image') . ' AND une!= ' . sql_quote('on'), '', 'date DESC', "0, $objets_a_trouver"));
		}

		if (in_array('evenement', $objets) && $config['evenement']['auto'] ?? '') {
			$limite_debut_futur = $config['evenement']['limite_debut_futur'];
			$date = new DateTime();
			$date->modify("+ $limite_debut_futur day");
			$date = 'date(\'' . $date->format('Y-m-d H:i:s') .'\')';
			$une = array_merge($une, sql_allfetsel('id_evenement AS id_objet, date_debut as date, \'evenement\' AS objet', 'spip_evenements', "$where_statut and date_debut <= $date AND date_debut > NOW() AND une!=" . sql_quote('on'), '', 'date DESC', "0, $objets_a_trouver"));
		}
	}


	// Puis trier
	usort($une, function ($a, $b) {
		if ($a['date'] < $b['date']) {
			return 1;
		} elseif ($a['date'] === $b['date']) {
			return 0;
		} else {
			return -1;
		}
	});

	// Et limiter
	$une = array_slice($une, 0, $quantite);
	// Ouf, boulot fini
	return $une;
}

/**
 * Retourne les objets de une qui ont un type précis
 * @param array $une
 * @param string $objet
 * @return array [int id_objet]
**/
function koinos_objets_une_avec_type(array $une, string $objet): array {
	$retour = [];
	foreach ($une as $i => $o) {
		if ($o['objet'] === $objet) {
			$retour[] = $o['id_objet'];
		}
	}
	return $retour;
}
