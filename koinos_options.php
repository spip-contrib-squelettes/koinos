<?php

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

if (!isset($GLOBALS['z_blocs'])) {
	$GLOBALS['z_blocs'] = [ 'content', 'aside', 'more', 'skiplink', 'head','head_js'];
}

define('_PAGINATION_NOMBRE_LIENS_MAX', 5);
