// Régler le scroll à cause de l'entete en fixed. Cf https://git.spip.net/spip/spip/commit/9f50bd16bfce863b0db3393ca27c0df6de6a251a]

jQuery.spip.positionner_marge = 49;

// Slider


$(document).ready(function() {
	$('.slider').each(
		function(){
			$(this).slick(
				{
					lazyLoad: 'progressive',
					slidesToShow: 1,
					slidesToScroll: 1,
					arrows: true,
					autoplay:true,
					autoplaySpeed:6000,
				}
			);
		}
	);
});
