<?php

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

include_spip('inc/cextras');
include_spip('base/koinos');

function koinos_upgrade($nom_meta_base_version, $version_cible) {

	$maj['create'] = [['koinos_configurer']];
	cextras_api_upgrade(koinos_declarer_champs_extras(), $maj['create']);
	$maj['12'] = [['koinos_configurer_coordonnees']];
	$maj['25'] = [['koinos_configurer']];
	cextras_api_upgrade(koinos_declarer_champs_extras(), $maj['26']);
	include_spip('base/upgrade');
	maj_plugin($nom_meta_base_version, $version_cible, $maj);
}

function koinos_configurer_coordonnees() {
	include_spip('inc/config');
	$config = lire_config('coordonnees');
	$config['objets'] = array_merge($config['objets'] ?? [], ['spip_articles', 'spip_auteurs', 'spip_albums']);
	ecrire_config('coordonnees', $config);
}

function koinos_vider_tables($nom_meta_base_version) {
	cextras_api_vider_tables(koinos_declarer_champs_extras());
	effacer_meta($nom_meta_base_version);
}

/**
 * Mettre une configuration par défaut
 **/
function koinos_configurer(): void {
	include_spip('inc/config');
	ecrire_config(
		'koinos',
		[
			'une' => [
				'quantite' => 2,
				'objets' => ['album', 'article'],
				'mode' => 'mixte',
				'album' => [
					'age' => 15,
					'auto' => 'on',
				],
				'article' => [
					'age' => 15,
					'auto' => 'on',
				],
				'document' => [
					'age' => 15,
					'auto' => '',
				],
				'evenement' => [
					'limite_debut_futur' => '365',
					'auto' => '',
				]
			],
			'lateral' => [
				'fil' => [
					'pagination' => 20,
					'age' => 30,
				]
			]
		]
	);
}
